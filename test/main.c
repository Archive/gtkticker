/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  Author:
  Ariel Rios <ariel@galway.com.mx>
 
This file is part of the GtkTicker library

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include <gnome.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtkticker.h>

gint 
main (gint argc, gchar **argv)
{
	GtkWidget *window;
	GtkWidget   *hbox;
	GtkWidget *ticker;
	GtkWidget *button;

	gnome_init ("gtkticker", "0.1", argc, argv);

	
	gdk_rgb_init ();
	gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	gtk_widget_set_default_visual (gdk_rgb_get_visual ());
	
	window = gtk_window_new (GTK_WINDOW_DIALOG);
	hbox   = gtk_hbox_new (TRUE, 1);
	ticker = gtk_ticker_new (20, 20);
	button = gtk_button_new_with_label ("Tick");

	gtk_box_pack_start (GTK_BOX (hbox), ticker, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
	gtk_container_add (GTK_CONTAINER (window), hbox);


	//gtk_ticker_append_author (ticker, "Ariel Rios", "ariel@arcavia.com");
	
	//gtk_timeout_add (1300, (GtkFunction) gtk_ticker_new_sparkles_timeout, ticker->canvas);
	gtk_timeout_add (50, (GtkFunction) gtk_ticker_scroll, (gpointer) ticker);

	gtk_widget_show_all (window);
	
	gtk_main ();

	return 0;
}


