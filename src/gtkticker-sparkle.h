/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  Author:
  Ariel Rios <ariel@galway.com.mx>

 
This library is part of the GtkTicker library

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

/*
  This library is intended for gtkticker private use
  If you try to use this functions on your programms
  you are probably wrong.

  The content of this code is taken from the gtk-about file
  included on the gnome-core package

  FIXME:
  Obviously I need to put here the correct copyrights notices...

*/

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkwidget.h>

typedef struct _Sparkle Sparkle;

#define NUM_FRAMES 8
#define DELTA 0.4


struct _Sparkle {
	GnomeCanvasItem *hline;
	GnomeCanvasItem *vline;
	GnomeCanvasItem *hsmall;
	GnomeCanvasItem *vsmall;
	
	GnomeCanvasPoints *hpoints [NUM_FRAMES];
	GnomeCanvasPoints *vpoints [NUM_FRAMES];
	GnomeCanvasPoints *hspoints [NUM_FRAMES];
	GnomeCanvasPoints *vspoints [NUM_FRAMES];

	gint count;
	gboolean up;
};

static void       gtk_ticker_sparkle_destroy      (Sparkle *sparkle);

static gboolean   gtk_ticker_sparkle_timeout      (Sparkle *sparkle);

static void       gtk_ticker_fill_points          (GnomeCanvasPoints *points,
						   double x, double y, double delta,
						   gboolean horizontal, gboolean square);

static void       gtk_ticker_sparkle_new          (GnomeCanvas *canvas, double x, double y);

static gint       gtk_ticker_new_sparkles_timeout (GnomeCanvas *canvas, gint index);  

static void       gtk_ticker_free_imlib_image     (GtkObject *object, gpointer data);
















