/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  Author:
  Ariel Rios <ariel@galway.com.mx>

  Author:
  Ariel Rios <ariel@galway.com.mx>
 
This library is part of the GtkTicker library

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/


#ifndef INC_GTK_EV_H
#define INC_GTK_EV_H

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkwidget.h>


#define GTK_TICKER(obj) GTK_CHECK_CAST(obj, gtk_ticker_get_type (), GtkTicker)
#define GTK_TICKER_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_ticker_get_type (), GtkTickerClass)
#define GTK_IS_TICKER(obj) GTK_CHECK_TYPE(obj, gtk_ticker_get_type ())

typedef struct _GtkTicker GtkTicker;
typedef struct _GtkTickerClass GtkTickerClass;

typedef struct _GtkTickerAuthor GtkTickerAuthor;

struct _GtkTicker
{
	GtkWidget    widget;
	
	GtkWidget    *canvas;
	GtkWidget    *area;
	GdkPixmap    *pixmap;
	GdkFont      *font;
	GdkFont      *italicfont;
	gint y, y_to_wrap_at;
	gint         howmuch;  
	GdkPixbuf    *im;

	GSList *authors;

	gboolean flag;
};

struct _GtkTickerClass
{
	GtkWidgetClass parent_class;
};
	
struct _GtkTickerAuthor
{
	const gchar *name;
	const gchar *email;
};

guint                  gtk_ticker_get_type             (void);
GtkWidget*             gtk_ticker_new                  (gint width, gint height);

gint                   gtk_ticker_scroll               (gpointer data);

/*
void                   gtk_ticker_set_background       (GtkTicker* ticker, GdkColor* color);
void                   gtk_ticker_set_font             (GtkTicker* ticker, GdkFont* font);

void                   gtk_ticker_set_title            (GtkTicker* ticker, gchar *title);
*/
void                   gtk_ticker_append_authors       (GtkTicker *ticker, 
							GtkTickerAuthor *authors);


/*
  Use these functions with interpreted language bindings like guile
*/


void                   gtk_ticker_append_author        (GtkTicker* ticker, gchar *name, gchar *email);



gint                   gtk_ticker_start                (GtkTicker *ticker, guint32 interval);


#endif









