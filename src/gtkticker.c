/* -*- mode: c; c-basic-offset: 8 -*- */

/*

  Copyright 2000 Ariel Rios

  Author:
  Ariel Rios <ariel@galway.com.mx>
 
  This file is part of the GtkTicker library

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/



#include "gtkticker.h"
#include "gtkticker-sparkle.h"

static void gtk_ticker_class_init (GtkTickerClass *klass);

static void gtk_ticker_init (GtkTicker *ticker);

/* GtkObject functions */

static void gtk_ticker_destroy (GtkObject *object);

/* GtkWidget functions */
static void gtk_ticker_realize (GtkWidget *widget);
static gboolean gtk_ticker_exposed (GtkWidget *widget, GdkEventExpose *event);
 
/* GtkTicker specific functions */


static GtkWidgetClass *parent_class = NULL;

guint
gtk_ticker_get_type (void)
{
	static guint ticker_type = 0;

	if (!ticker_type)
	{
		static const GtkTypeInfo ticker_info =
		{
			"GtkTicker",
			sizeof (GtkTicker),
			sizeof (GtkTickerClass),
			(GtkClassInitFunc) gtk_ticker_class_init,
			(GtkObjectInitFunc) gtk_ticker_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL,
		};

		ticker_type = gtk_type_unique (gtk_widget_get_type (), &ticker_info);
	}
	
	return ticker_type;
}

			
static void
gtk_ticker_class_init (GtkTickerClass *klass)
{
	GtkTickerClass *ticker_class;
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	ticker_class = (GtkTickerClass *) klass;
	object_class = (GtkObjectClass*) klass;
	widget_class = (GtkWidgetClass*) klass;

	object_class->destroy = gtk_ticker_destroy;

	parent_class = gtk_type_class (GTK_TYPE_WIDGET);

	widget_class->realize = gtk_ticker_realize;
	//widget_class->unrealize = gtk_ticker_unrealize;
	widget_class->expose_event = gtk_ticker_exposed;
}

static void
gtk_ticker_init (GtkTicker *ticker)
{
        GTK_WIDGET_SET_FLAGS (GTK_WIDGET (ticker), GTK_CAN_FOCUS);
	GTK_WIDGET_SET_FLAGS (ticker, GTK_NO_WINDOW);

	ticker->canvas       = NULL;
        ticker->area         = NULL;
        ticker->pixmap       = NULL;
        ticker->font         = NULL;
        ticker->italicfont   = NULL;
        ticker->y            = 0;
        ticker->y_to_wrap_at = 0;
        ticker->howmuch      = 0;
        ticker->im           = NULL;
        ticker->authors      = NULL;
        ticker->flag         = TRUE;
          
}


GtkWidget* 
gtk_ticker_new (gint width, gint height)
{
	GtkTicker* ticker;
	
	ticker = gtk_type_new (gtk_ticker_get_type ());

	/*
	  FIXME:
	  Should these go here?
	*/

	
	  gdk_rgb_init ();
	  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	  gtk_widget_set_default_visual (gdk_rgb_get_visual ());
	
	
	ticker->font = gdk_font_load ("-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*");
	ticker->italicfont = gdk_font_load ("-adobe-helvetica-medium-o-normal--*-120-*-*-*-*-*-*");

	if (!ticker->font)
		g_error ("Can't find font");

	if (!ticker->italicfont)
		g_error ("Can't find font");


	ticker->canvas = gnome_canvas_new ();
	ticker->area = gtk_drawing_area_new ();
	/*ticker->pixmap = gdk_pixmap_new (ticker->area,
				 width,
				 height,
				 -1);
	*/

	/*
	ticker->image = gnome_canvas_item_new (GNOME_CANVAS_GROUP (GNOME_CANVAS (canvas)->root),
				       gnome_canvas_pixbuf_get_type (),
				       "pixbuf", im,
				       "x", 0.0,
				       "y", 0.0,
				       "width", (double) gdk_pixbuf_get_width (im),
				       "height", (double) gdk_pixbuf_get_height (im),
			       NULL);
	*/
/* 
   FIXME:
   Do we need these?
*/
	gtk_widget_set_usize (ticker->canvas, width, height);
	gnome_canvas_set_scroll_region (GNOME_CANVAS (ticker->canvas), 0, 0, width, height);

	gtk_drawing_area_size (GTK_DRAWING_AREA (ticker->area), 320, 160);
	gtk_widget_draw (ticker->area, NULL);


	return GTK_WIDGET (ticker);
}                                   

/* GtkObject Functions */

static void
gtk_ticker_destroy (GtkObject *obj)
{

	GtkTicker *ticker;
	GSList *ls;
	
	g_return_if_fail (obj != NULL);
	g_return_if_fail (GTK_IS_TICKER (obj));

	ticker = GTK_TICKER (obj);
	ls = ticker->authors;

	g_slist_free (ticker->authors);
 
	ticker->area         = NULL;
	ticker->pixmap       = NULL;
	ticker->font         = NULL;
	ticker->italicfont   = NULL;
	ticker->y            = 0;
	ticker->y_to_wrap_at = 0;
	ticker->howmuch      = 0;  
	ticker->im           = NULL;
	ticker->authors      = NULL;
	ticker->flag         = FALSE;

	g_slist_free (ls);

	/* 
	   FIXME: Correct horrible memory leak!
	*/

		if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (obj);

}

static void
gtk_ticker_realize (GtkWidget *widget)
{
	GtkTicker *ticker;
	gint attributes_mask;
	GdkWindowAttr attributes;

        g_return_if_fail (widget != NULL);
        g_return_if_fail (GTK_IS_TICKER (widget));

	//GTK_WIDGET_NO_WINDOW (widget);

	ticker = GTK_TICKER (widget);

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	 if (GTK_WIDGET_CLASS (parent_class)->realize)
                (* GTK_WIDGET_CLASS (parent_class)->realize) (widget);

	 attributes.window_type = GDK_WINDOW_CHILD;
	 attributes.x = widget->allocation.x;
	 attributes.y = widget->allocation.y;
	 attributes.width = widget->allocation.width;
	 attributes.height = widget->allocation.height;
	 attributes.wclass = GDK_INPUT_OUTPUT;
         attributes.visual = gtk_widget_get_visual (widget);
         attributes.colormap = gtk_widget_get_colormap  (widget);
         attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK; 
         attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;         
	 widget->window = gdk_window_new (gtk_widget_get_parent_window (widget), 
					  &attributes, attributes_mask);
         gdk_window_set_user_data (widget->window, widget);        
         
}

static gboolean
gtk_ticker_exposed (GtkWidget *widget, GdkEventExpose *event)
{

	GtkTicker *ticker;

	/*
	  g_return_if_fail (widget != NULL);
	  g_return_if_fail (GTK_IS_TICKER (widget));
	*/
	
	ticker = (GTK_TICKER (widget));

	gdk_draw_pixmap (widget->window,
			 widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
			 ticker->pixmap,
			 event->area.x, event->area.y,
			 event->area.x, event->area.y,
			 event->area.width, event->area.height);
	return TRUE;
}


/* 
FIXME
The following code is taken from the gnome-about.c file
located under the gnome-core package and adapated to gtkticker
*/


gint
gtk_ticker_scroll (gpointer data)
{
	gint cury, i, totalwidth;
	GdkRectangle update_rect;
	GtkTicker *ticker = (GtkTicker *) data;
	

	
	g_return_if_fail (ticker != NULL);
	g_return_if_fail (GTK_IS_TICKER(ticker));
	

/*
	ticker->y_to_wrap_at = -(sizeof (ticker->authors) / sizeof (ticker->authors [0]))*
		(ticker->font->ascent + ticker->font->descent);

	
	int = 0;
	cury = ticker->y;
*/
	
	printf ("estoy aqui");

	ticker->pixmap = gdk_pixmap_new (ticker->area,
	                                10,10,
                                 -1);
        

	gdk_draw_rectangle (ticker->pixmap, ticker->area->style->black_gc,
			    TRUE,
			    0, 0,
			    ticker->area->allocation.width,
			    ticker->area->allocation.height);
	
/*
	while (ticker->authors->next != NULL) 
	{
		
		totalwidth = gdk_string_width (ticker->font, ticker->authors [i].name);
		if (ticker->authors [i].email)
			totalwidth += 4 + gdk_string_width (ticker->italicfont, ticker->authors [i].email);

		if(cury > -ticker->font->descent &&
		   cury < ticker->area->allocation.height + ticker->font->ascent) {
		        gdk_draw_string (ticker->pixmap,
					 ticker->font,
					 ticker->area->style->white_gc,
					 (ticker->area->allocation.width - 
					  totalwidth) / 2,
					 cury,
					 _(ticker->authors [i].name));
			gdk_draw_string (ticker->pixmap,
					 ticker->italicfont,
					 ticker->area->style->white_gc,
					 (ticker->area->allocation.width -
					  totalwidth) / 2  + 
					 (ticker->authors [i].email ? 4 :0) +
					 gdk_string_width (ticker->font, ticker->authors [i].name),
					 cury,
					 _(ticker->authors [i].email));
		}

		i++;
		cury += ticker->font->ascent + ticker->font->descent;

	}

	ticker->y --;
	if (ticker->y < ticker->y_to_wrap_at)
	        ticker->y = ticker->area->allocation.height + ticker->font->ascent;

	update_rect.x = 0;
	update_rect.y = 0;
	update_rect.width = ticker->area->allocation.width;
	update_rect.height = ticker->area->allocation.height;
		*/
	
		printf ("it works \n");

	//gtk_widget_draw (ticker->area, &update_rect);
	
	return TRUE;

}


void 
gtk_ticker_append_authors (GtkTicker *ticker,
			   GtkTickerAuthor *authors)
{
	g_return_if_fail (ticker != NULL);
	g_return_if_fail (GTK_IS_TICKER(ticker));
	
/* 
   FIXME 
*/

	

}

void 
gtk_ticker_append_author (GtkTicker *ticker, gchar *name, gchar *email)
{
	GtkTickerAuthor *author;

	g_return_if_fail (ticker != NULL);
	g_return_if_fail (GTK_IS_TICKER(ticker));
	author->name = (gchar *) g_malloc (sizeof (gchar) * strlen (name));
	author->name =  name;
	author->email = (gchar *) g_malloc (sizeof (gchar) * strlen (email));
	author->email = email;

	g_slist_prepend (ticker->authors, author);
}

gint
gtk_ticker_start (GtkTicker *ticker, guint32 interval)
{

	if (ticker->flag)
		return FALSE;

	g_return_if_fail (ticker != NULL);
	g_return_if_fail (GTK_IS_TICKER(ticker));

	ticker->flag = FALSE;


	gtk_timeout_add (interval , (GtkFunction) gtk_ticker_scroll, (gpointer) ticker);

	return TRUE;
}








