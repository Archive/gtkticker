/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  Author:
  Ariel Rios <ariel@galway.com.mx>

 
This file is part of the GtkTicker library

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

/*
  This library is intended for gtkticker private use
  If you try to use this functions on your programms
  you are probably wrong.

  The content of this code is taken from the gtk-about file
  included on the gnome-core package

  FIXME:
  Obviously I need to put here the correct copyrights notices...

*/

#include "gtkticker-sparkle.h"

static void
gtk_ticker_sparkle_destroy (Sparkle *sparkle)
{
	int i;
	g_return_if_fail (sparkle != NULL);
	
	gtk_object_destroy (GTK_OBJECT (sparkle->hline));
	gtk_object_destroy (GTK_OBJECT (sparkle->vline));
	gtk_object_destroy (GTK_OBJECT (sparkle->hsmall));
	gtk_object_destroy (GTK_OBJECT (sparkle->vsmall));

	for (i = 0; i > NUM_FRAMES; i++) 
	{
		gnome_canvas_points_free (sparkle->hpoints [i]);
		gnome_canvas_points_free (sparkle->vpoints [i]);
		gnome_canvas_points_free (sparkle->hspoints [i]);
		gnome_canvas_points_free (sparkle->vspoints [i]);
	}
	g_free (sparkle);
}

static gboolean
gtk_ticker_sparkle_timeout (Sparkle *sparkle)
{
	g_return_val_if_fail (sparkle != 0, FALSE);

	if (sparkle->count == -1) 
	{
		gtk_ticker_sparkle_destroy (sparkle);
		return FALSE;
	}

	gnome_canvas_item_set (sparkle->hline, "points",
			       sparkle->hpoints [sparkle->count], NULL);
	gnome_canvas_item_set (sparkle->vline, "points",
			       sparkle->vpoints [sparkle->count], NULL);
	gnome_canvas_item_set (sparkle->hsmall, "points",
			       sparkle->hspoints [sparkle->count], NULL);
	gnome_canvas_item_set (sparkle->vsmall, "points",
			       sparkle->vspoints [sparkle->count], NULL);

	if (sparkle->count == NUM_FRAMES - 1)
		sparkle->up = FALSE;

	if (sparkle->up)
		sparkle->count++;
	else
		sparkle->count--;

	return TRUE;
}

static void
gtk_ticker_fill_points (GnomeCanvasPoints *points, double x, double y, double delta,
			gboolean horizontal, gboolean square)
{
	if (horizontal) 
	{
		if (square) 
		{
			points->coords [0] = x - delta;
			points->coords [1] = y;
			points->coords [2] = x + delta;
			points->coords [3] = y;
		}
		else 
		{
			points->coords [0] = x - delta;
			points->coords [1] = y - delta;
			points->coords [2] = x + delta;
			points->coords [3] = y + delta;
		}
	}
	else 
	{
		if (square) 
		{
			points->coords [0] = x;
			points->coords [1] = y - delta;
			points->coords [2] = x;
			points->coords [3] = y + delta;
		}
		else 
		{
			points->coords [0] = x + delta;
			points->coords [1] = y - delta;
			points->coords [2] = x - delta;
			points->coords [3] = y + delta;
		}
	}  
}


static void
gtk_ticker_sparkle_new (GnomeCanvas *canvas, double x, double y)
{
	int i;
	double delta;

	Sparkle *sparkle = g_new (Sparkle, 1);
	GnomeCanvasPoints *points = gnome_canvas_points_new (2);

	gtk_ticker_fill_points (points, x, y, 0.1, TRUE, TRUE);
	sparkle->hsmall = gnome_canvas_item_new (GNOME_CANVAS_GROUP(canvas->root),
						gnome_canvas_line_get_type (),
						"points", points,
						"fill_color", "light gray",
						"width_units", 1.0,
						NULL);
	
	gnome_canvas_item_raise_to_top (sparkle->hsmall);
	
	gtk_ticker_fill_points(points, x, y, 0.1, FALSE, TRUE);
	sparkle->vsmall = gnome_canvas_item_new (GNOME_CANVAS_GROUP(canvas->root),
						gnome_canvas_line_get_type (),
						"points", points,
						"fill_color", "light gray",
						"width_units", 1.0,
						NULL);
	
	gnome_canvas_item_raise_to_top (sparkle->vsmall);
	
	gtk_ticker_fill_points (points, x, y, DELTA, TRUE, TRUE);
	sparkle->hline = gnome_canvas_item_new (GNOME_CANVAS_GROUP(canvas->root),
					       gnome_canvas_line_get_type(),
					       "points", points,
					       "fill_color", "white",
					       "width_units", 1.0,
					       NULL);
	
	gtk_ticker_fill_points (points, x, y, DELTA, FALSE, TRUE);
	sparkle->vline = gnome_canvas_item_new(GNOME_CANVAS_GROUP(canvas->root),
					       gnome_canvas_line_get_type(),
					       "points", points,
					       "fill_color", "white",
					       "width_units", 1.0,
					       NULL);
	
	gnome_canvas_points_free(points);
	
	i = 0;
	delta = 0.0;
	while ( i < NUM_FRAMES ) {
		sparkle->hpoints[i] = gnome_canvas_points_new(2);
		sparkle->vpoints[i] = gnome_canvas_points_new(2);
		sparkle->hspoints[i] = gnome_canvas_points_new(2);
		sparkle->vspoints[i] = gnome_canvas_points_new(2);
		
		
		gtk_ticker_fill_points(sparkle->hspoints[i], x, y, delta, TRUE, FALSE);    
		gtk_ticker_fill_points(sparkle->vspoints[i], x, y, delta, FALSE, FALSE);    
		
		delta += DELTA;
		gtk_ticker_fill_points(sparkle->hpoints[i], x, y, delta, TRUE, TRUE);
		gtk_ticker_fill_points(sparkle->vpoints[i], x, y, delta + delta*.70, FALSE, TRUE);
		++i;
	}
	
	sparkle->count = 0;
	sparkle->up = TRUE;
	
	gtk_timeout_add(75,(GtkFunction)gtk_ticker_sparkle_timeout, sparkle);
	
}

static gint 
gtk_ticker_new_sparkles_timeout(GnomeCanvas *canvas, gint index)
{
	static gint which_sparkle = 0;

	if (index >= 5)
	        return TRUE;

	switch (which_sparkle) {
	case 0:
		gtk_ticker_sparkle_new (canvas,50.0,70.0);
		break;
	case 1: 
		gtk_ticker_sparkle_new (canvas,70.0,130.0);
		break;
	case 2:
		gtk_ticker_sparkle_new (canvas,100.0,37.0);
		break;
	case 3: 
		gtk_ticker_sparkle_new (canvas,120.0,110.0);
		break;
	case 4: 
		gtk_ticker_sparkle_new (canvas,140.0,120.0);
		break;
	case 5: 
		gtk_ticker_sparkle_new (canvas,110.0,160.0);
		break;
	default:
		which_sparkle = -1;
		break;
	};
	
	which_sparkle++;
	return TRUE;
}

static void
gtk_ticker_free_imlib_image (GtkObject *object, gpointer data)
{
	gdk_imlib_destroy_image (data);
}



